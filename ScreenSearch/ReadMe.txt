image samples are screenshots of webpages

javaPageSample is a screencap of http://www.java.com/en/download/installed8.jsp, taken with screenSearch itself

aardvarkSample is a screencap of https://en.wikipedia.org/wiki/Aardvark#Description that was cropped to include just the "description" section.